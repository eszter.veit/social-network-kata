import pytest

from social_network.exceptions import InvalidMessageException
from social_network.pattern import ActionPatterns, get_action_for_message


def test_post():
    assert get_action_for_message("Alice -> This is the Post") == ActionPatterns.post


def test_like():
    assert get_action_for_message("Alice likes John") == ActionPatterns.like


def test_unlike():
    assert get_action_for_message("Alice unlike John") == ActionPatterns.unlike


def test_invalid():
    with pytest.raises(InvalidMessageException):
        get_action_for_message("This is not a valid one")


