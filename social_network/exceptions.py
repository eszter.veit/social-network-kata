class InvalidMessageException(Exception):
    def __init__(self, input_message):
        self.message = f"{input_message} - message is invalid"
        super().__init__(self.message)


class InvalidUserException(Exception):
    def __init__(self, user):
        self.message = f"{user} - does not exist"
        super().__init__(self.message)
