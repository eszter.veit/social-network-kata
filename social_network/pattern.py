import re
from enum import Enum

from social_network.exceptions import InvalidMessageException


class ActionPatterns(Enum):

    post = "[A-Za-z]+ -> .+"
    read = "[A-Za-z]+"
    following = "[A-Za-z]+ follows [A-Za-z]+"
    wall = "[A-Za-z]+ wall"
    like = "[A-Za-z]+ likes [A-Za-z]+"
    unlike = "[A-Za-z]+ unlike [A-Za-z]+"


def get_action_for_message(message: str) -> ActionPatterns:

    for pattern in ActionPatterns:
        _rex = re.compile(pattern.value)
        if _rex.fullmatch(message):
            return pattern

    raise InvalidMessageException(message)


def find_strings_in_message(message: str):
    return re.findall(pattern="[A-Za-z]+", string=message)