import sqlite3

from social_network.models import User


class SocalDatabase:

    def __init__(self):
        self.db = sqlite3.connect(":memory:")
        self.create_tables()
        self._create_defaults()

    def create_tables(self) -> None:
        self.db.execute("CREATE table user (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,name TEXT )")
        self.db.execute(
            "CREATE table post (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,text TEXT,created TIMESTAMP , user_id INTEGER, FOREIGN KEY (user_id) REFERENCES user(id))")
        self.db.execute(
        "CREATE table like (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, user_id INTEGER, post_id INTEGER ,"
        "FOREIGN KEY (user_id) REFERENCES user(id), FOREIGN KEY (post_id) REFERENCES post(id))")
        self.db.execute(
            "CREATE table follower (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, user_id_follower INTEGER, user_id_to_follow INTEGER ,"
            "FOREIGN KEY (user_id_follower) REFERENCES user(id), FOREIGN KEY (user_id_to_follow) REFERENCES post(id))")

    def _create_defaults(self) -> None:
        self.db.execute("INSERT INTO user (name) VALUES ('Alice')")
        self.db.execute("INSERT INTO user (name) VALUES ('Charlie')")
        self.db.execute("INSERT INTO user (name) VALUES ('Bob')")

        self.db.execute("INSERT INTO post (text,created,user_id) VALUES ('This post is default',(STRFTIME('%Y-%m-%d %H:%M:%S', 'NOW')), 1)")
        self.db.execute("INSERT INTO post (text,created,user_id) VALUES ('This post is another default',(STRFTIME('%Y-%m-%d %H:%M:%S', 'NOW')), 1)")
        self.db.execute("INSERT INTO post (text,created,user_id) VALUES ('boring default post', (STRFTIME('%Y-%m-%d %H:%M:%S', 'NOW')),2)")

    def get_user(self, name: str) -> User | None:
        result = self.db.execute("Select name,id from user where name = ?", (name,)).fetchone()
        if result:
            return User(name=result[0], id=result[1])

    def create_post_to_user(self, text, user_id):
        self.db.execute("INSERT INTO post (text,created,user_id) VALUES (?,(STRFTIME('%Y-%m-%d %H:%M:%S', 'NOW')),?)", (text, user_id))

    def get_posts_by_user(self, user_id: int) -> list | None:
        return self.db.execute("Select text,created from post where user_id = ?", (user_id,)).fetchall()
