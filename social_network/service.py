from social_network.exceptions import InvalidUserException
from social_network.pattern import get_action_for_message, ActionPatterns, find_strings_in_message
from social_network.storage.database import SocalDatabase


def handle_message(message: str, db: SocalDatabase):
    pattern = get_action_for_message(message)
    return handle_action(pattern, message, db)


def handle_action(pattern: ActionPatterns, message: str, db: SocalDatabase) -> str | list:
    if pattern == ActionPatterns.post: return create_post(message, db)
    if pattern == ActionPatterns.read: return get_wall_of_user(message, db)


def create_post(message: str, db: SocalDatabase) -> str:
    texts = find_strings_in_message(message)
    user = db.get_user(texts[0])
    if not user:
        raise InvalidUserException(texts[0])

    print(user, texts[1:])

    db.create_post_to_user(user_id=user.id, text=" ".join(texts[1:]))

    return message


def get_wall_of_user(message: str, db: SocalDatabase) -> list | None:
    texts = find_strings_in_message(message)
    user = db.get_user(texts[0])
    if not user:
        raise InvalidUserException(texts[0])
    return db.get_posts_by_user(user.id)
    # fmt = '%Y-%m-%d %H:%M:%S'

    # minutes_diff = (datetime.now() - datetime.strptime("2022-05-09 15:55:02",fmt)).total_seconds() / 60.0


def create_echo_statement():
    pass
