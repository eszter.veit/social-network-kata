import typer

from social_network.service import handle_message
from social_network.storage.database import SocalDatabase


def main():
    typer.echo("press q to quit!")
    typer.echo("Starting db")
    typer.echo("----------------------------------------")
    db = SocalDatabase()

    while True:
        message = input()

        if message == 'q':
            typer.echo("Adios")
            exit()

        handled = handle_message(message, db)
        if type(handled) is list:
            for m in handled:
                typer.echo(m)
        else:
            typer.echo(message)


def start_app():
    typer.echo("Hello Social")
    typer.run(main)



